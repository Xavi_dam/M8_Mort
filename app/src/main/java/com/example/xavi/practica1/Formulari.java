package com.example.xavi.practica1;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import java.util.Calendar;
import java.util.Random;

public class Formulari extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    Button resultat;
    private EditText fromDateEtxt;
    private DatePickerDialog fromDatePickerDialog;
    private static final Random rgenerator = new Random();
    CheckBox futbol, basket, alcohol, drugs, molestar, motor, paraca, voyeur, sense, sexe, cine, robar;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulari);
        final Intent check = new Intent(Formulari.this, Result.class);
        futbol = (CheckBox) findViewById(R.id.futbol);
        futbol.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("fut", b);
                check.putExtras(bundle);
            }
        });
        basket = (CheckBox) findViewById(R.id.basket);
        basket.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("bas", b);
                check.putExtras(bundle);
            }
        });
        alcohol = (CheckBox) findViewById(R.id.alcohol);
        alcohol.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("alc", b);
                check.putExtras(bundle);
            }
        });
        drugs = (CheckBox) findViewById(R.id.drugs);
        drugs.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("drug", b);
                check.putExtras(bundle);
            }
        });
        molestar = (CheckBox) findViewById(R.id.molestar);
        molestar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("mols", b);
                check.putExtras(bundle);
            }
        });
        motor = (CheckBox) findViewById(R.id.motor);
        motor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("mot", b);
                check.putExtras(bundle);
            }
        });
        paraca = (CheckBox) findViewById(R.id.paraca);
        paraca.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("par", b);
                check.putExtras(bundle);
            }
        });
        voyeur = (CheckBox) findViewById(R.id.voyeur);
        voyeur.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("voy", b);
                check.putExtras(bundle);
            }
        });
        sense = (CheckBox) findViewById(R.id.sense);
        sense.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("sen", b);
                check.putExtras(bundle);
            }
        });
        sexe = (CheckBox) findViewById(R.id.sexe);
        sexe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("sex", b);
                check.putExtras(bundle);
            }
        });
        cine = (CheckBox) findViewById(R.id.cine);
        cine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("cin", b);
                check.putExtras(bundle);
            }
        });
        robar = (CheckBox) findViewById(R.id.robar);
        robar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("rob", b);
                check.putExtras(bundle);
            }
        });
        resultat = (Button) findViewById(R.id.btn1);
        resultat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultat = new Intent(Formulari.this, Result.class);
                startActivity(resultat);
                startActivity(check);
            }
        });

        fromDateEtxt = (EditText) findViewById(R.id.data);
        fromDateEtxt.setInputType(InputType.TYPE_NULL);
        fromDateEtxt.requestFocus();

            /* Ponemos el Listener al EditText */
        fromDateEtxt.setOnClickListener(this);
            /* Creamos el DatePickerDialog a partir de la fechaActual */
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, this, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }

    @Override
    public void onClick(View v) {
        if (v == fromDateEtxt) {
            fromDatePickerDialog.show();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

            /* Es mejor utilizar el SimpleDateFormat, sólo para ponerlo fácil */

        String fecha = dayOfMonth + "/" + month + 1 + "/" + year;
        fromDateEtxt.setText(fecha);

        }

    }




