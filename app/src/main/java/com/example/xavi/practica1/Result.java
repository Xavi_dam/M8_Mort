package com.example.xavi.practica1;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

public class Result extends AppCompatActivity  {

    private String[] myString;
    private static final Random rgenerator = new Random();
    Calendar unaFecha;
    Random aleatorio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Bundle bundle = getIntent().getExtras();
        Boolean futbol = bundle.getBoolean("fut");
        Boolean basket = bundle.getBoolean("bas");
        Boolean sexe = bundle.getBoolean("sex");
        Boolean drugs = bundle.getBoolean("drug");
        Boolean molest = bundle.getBoolean("mols");
        Boolean paraca = bundle.getBoolean("par");
        Boolean motor = bundle.getBoolean("mot");
        Boolean robar = bundle.getBoolean("rob");
        Boolean sense = bundle.getBoolean("sen");
        Boolean voyeur = bundle.getBoolean("voy");
        Boolean cine = bundle.getBoolean("cin");
        Boolean alco = bundle.getBoolean("alc");

        if (futbol & basket) {
            Resources res = getResources();
            myString = res.getStringArray(R.array.esport);
            String q = myString[rgenerator.nextInt(myString.length)];
            TextView tv = (TextView) findViewById(R.id.mort);
            tv.setText(q);
            aleatorio = new Random();
            unaFecha = Calendar.getInstance();
            unaFecha.set(aleatorio.nextInt(50) + 2018, aleatorio.nextInt(12) + 1, aleatorio.nextInt(30) + 1);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            TextView data = (TextView) findViewById(R.id.data);
            data.setText(sdf.format(unaFecha.getTime()));
            }
        else if (drugs & alco){
            Resources res = getResources();
            myString = res.getStringArray(R.array.vici);
            String q = myString[rgenerator.nextInt(myString.length)];
            TextView tv = (TextView) findViewById(R.id.mort);
            tv.setText(q);
            aleatorio = new Random();
            unaFecha = Calendar.getInstance();
            unaFecha.set(aleatorio.nextInt(5) + 2018, aleatorio.nextInt(12) + 1, aleatorio.nextInt(30) + 1);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            TextView data = (TextView) findViewById(R.id.data);
            data.setText(sdf.format(unaFecha.getTime()));
        }

        else if (sexe & voyeur){
            Resources res = getResources();
            myString = res.getStringArray(R.array.marrano);
            String q = myString[rgenerator.nextInt(myString.length)];
            TextView tv = (TextView) findViewById(R.id.mort);
            tv.setText(q);
            aleatorio = new Random();
            unaFecha = Calendar.getInstance();
            unaFecha.set(aleatorio.nextInt(30) + 2018, aleatorio.nextInt(12) + 1, aleatorio.nextInt(30) + 1);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            TextView data = (TextView) findViewById(R.id.data);
            data.setText(sdf.format(unaFecha.getTime()));
        }

        else if (paraca & motor){
            Resources res = getResources();
            myString = res.getStringArray(R.array.boig);
            String q = myString[rgenerator.nextInt(myString.length)];
            TextView tv = (TextView) findViewById(R.id.mort);
            tv.setText(q);
            aleatorio = new Random();
            unaFecha = Calendar.getInstance();
            unaFecha.set(aleatorio.nextInt(10) + 2018, aleatorio.nextInt(12) + 1, aleatorio.nextInt(30) + 1);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            TextView data = (TextView) findViewById(R.id.data);
            data.setText(sdf.format(unaFecha.getTime()));
        }

        else if (molest & robar){
            Resources res = getResources();
            myString = res.getStringArray(R.array.delicte);
            String q = myString[rgenerator.nextInt(myString.length)];
            TextView tv = (TextView) findViewById(R.id.mort);
            tv.setText(q);
            aleatorio = new Random();
            unaFecha = Calendar.getInstance();
            unaFecha.set(aleatorio.nextInt(25) + 2018, aleatorio.nextInt(12) + 1, aleatorio.nextInt(30) + 1);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            TextView data = (TextView) findViewById(R.id.data);
            data.setText(sdf.format(unaFecha.getTime()));
        }

        else if (sense & cine){
            Resources res = getResources();
            myString = res.getStringArray(R.array.avorrit);
            String q = myString[rgenerator.nextInt(myString.length)];
            TextView tv = (TextView) findViewById(R.id.mort);
            tv.setText(q);
            aleatorio = new Random();
            unaFecha = Calendar.getInstance();
            unaFecha.set(aleatorio.nextInt(45) + 2018, aleatorio.nextInt(12) + 1, aleatorio.nextInt(30) + 1);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            TextView data = (TextView) findViewById(R.id.data);
            data.setText(sdf.format(unaFecha.getTime()));
        }

        else {
            Resources res = getResources();
            myString = res.getStringArray(R.array.normal);
            String q = myString[rgenerator.nextInt(myString.length)];
            TextView tv = (TextView) findViewById(R.id.mort);
            tv.setText(q);
            aleatorio = new Random();
            unaFecha = Calendar.getInstance();
            unaFecha.set(aleatorio.nextInt(20) + 2018, aleatorio.nextInt(12) + 1, aleatorio.nextInt(30) + 1);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            TextView data = (TextView) findViewById(R.id.data);
            data.setText(sdf.format(unaFecha.getTime()));
        }
    }
}




